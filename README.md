run a roscore, topic publisher, topic listener from their own docker containers, orchestrated by docker-compose

You'll want on your dev host machine:

- ROS
- docker
- docker compose

to create new node

```bash
mkdir -p new_node/src
cd new_node
catkin_make
```

To add the workspace to your ROS environment you need to source the generated setup file:

```
$ . new_node/devel/setup.bash
```

to create package

cd new_node/src
catkin_create_pkg talker std_msgs roscpp

add cpp files to /src

add lines to CMakelists.txt in package:

add_executable(listener src/listener.cpp)
target_link_libraries(listener ${catkin_LIBRARIES})

# http://docs.ros.org/en/kinetic/api/catkin/html/howto/format2/run_tests.html

catkin_make run_tests_talker_gtest_test_your_node

sudo apt-get install python-rosdep

sudo apt install python-coverage
sudo apt install lcov

rosinstall .

catkin_make -DENABLE_COVERAGE_TESTING=ON -DCMAKE_BUILD_TYPE=Debug
catkin_make -DENABLE_COVERAGE_TESTING=ON -DCMAKE_BUILD_TYPE=Debug talker_coverage_report

# Goals/Features

- [x] monorepo
- [x] one node per container
- [x] containerized roscore
- [x] docker-compose
- [x] integrated unit tests
- [x] code coverage
- [ ] CICD
- [ ] failed unit tests fails CICD
- [ ] code coverage threshold enforced in CICD
- [ ] TDD workflow (live unit test reload)
- [ ] doxygen enabled
- [ ] ansible box setup
