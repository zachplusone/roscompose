#include <iostream>
#include <gtest/gtest.h>
#include "../src/functions/functions.h"

using namespace std;

TEST(add, should_return_4_given_2_and_2)
{
  ASSERT_EQ(add(2, 2), 4);
}

// TEST(SayHello, shouldSayHello)
// {
//   ASSERT_EQ(sayHello(), "hello");
// }

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}