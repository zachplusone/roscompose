#! /bin/bash
# Run me directly with sudo bash installs.bash

echo;echo
echo 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥  
echo 🔥 🔥 🔥  STARTING CLICKONE-S DEV SETUP 🔥 🔥 🔥 
echo 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥 🔥;echo; echo;

OS_DISTRO=$(lsb_release -cs)

case $OS_DISTRO in

  "focal")
    ROS_DISTRO="noetic"
    ;;

  "bionic")
    ROS_DISTRO="melodic"
    ;;

  "xenial")
    ROS_DISTRO="kinetic"
    ;;

  *)
    echo "Hmmm...can't detect your Ubuntu version. Exiting!"
    exit
    ;;
esac

echo 🖥 OS detected: $OS_DISTRO
echo 🤖 ROS distro to use: $ROS_DISTRO; echo; echo

if [ "$EUID" -ne 0 ]
  then echo "🔒 Yikes! Please run this script as root! 🔒"
  exit
fi


# # GIT
# # https://git-scm.com/download/linux
# echo 🔥 INSTALLING GIT
# add-apt-repository -y ppa:git-core/ppa
# apt-get update
# apt-get install -y git
# git --version
# echo 🚀 GIT INSTALLED: $(git version);echo;echo

# # DOCKER
# # https://docs.docker.com/install/linux/docker-ce/ubuntu/
# echo 🔥 INSTALLING DOCKER
# apt-get remove -y docker docker-engine docker.io containerd runc
# apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add 
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# apt-get update
# apt-get install docker-ce docker-ce-cli containerd.io
# usermod -aG docker $USER
# docker --version
# echo 🚀 DOCKER INSTALLED: $(docker --version);echo;echo

# # DOCKER-COMPOSE
# # https://docs.docker.com/compose/install/
# # To install a different version of Compose, substitute 1.25.0 with the version of Compose you want to use
# echo 🔥 INSTALLING DOCKER-COMPOSE
# curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# chmod +x /usr/local/bin/docker-compose
# docker-compose --version
# echo 🚀 DOCKER-COMPOSE INSTALLED: $(docker-compose --version);echo;echo

# # NVM (nodejs)
# # https://github.com/nvm-sh/nvm
# echo 🔥 INSTALLING NODEJS
# curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# nvm install 12.14.0
# node --version
# echo 🚀 NODEJS INSTALLED: $(node --version);echo;echo

# # YARN
# # https://yarnpkg.com/lang/en/docs/install/#debian-stable
# echo 🔥 INSTALLING YARN
# curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
# echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
# apt-get update && apt install -y yarn
# yarn --version
# echo 🚀 YARN INSTALLED: $(yarn --version);echo;echo

# # VS CODE
# # https://code.visualstudio.com/docs/setup/linux
# echo 🔥 INSTALLING VS CODE
# snap install --classic code 
# code --version
# echo 🚀 VS CODE INSTALLED: $(code --user-data-dir /tmp --version);echo;echo # /tmp avoids 'sudo' complaint

# wstool
echo 🔥 INSTALLING WSTOOL
apt-get install python3-wstool
echo 🚀 WSTOOL INSTALLED

# ROS
# https://wiki.ros.org/kinetic/Installation/Ubuntu
echo 🔥 INSTALLING ROS
echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list
apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
apt-get update
apt-get install ros-"$ROS_DISTRO"-desktop-full
echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> ~/.bashrc
source ~/.bashrc
echo 🚀 ROS INSTALLED:"$ROS_DISTRO";echo;echo

echo 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 
echo 🚀 🚀 🚀 CLICKONE-S DEV SETUP COMPLETE 🚀 🚀 🚀 🚀
echo 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀; echo;
echo 🚀 GIT: $(git version)
echo 🚀 DOCKER: $(docker --version)
echo 🚀 DOCKER-COMPOSE: $(docker-compose --version)
echo 🚀 NODEJS: $(node --version)
echo 🚀 VS CODE: $(code --user-data-dir /tmp --version) # /tmp avoids 'sudo' complaint
echo 🚀 ROS: $ROS_DISTRO
echo;echo
echo 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 
echo 🚀 🚀 🚀 NOW: LOG OUT AND IN AGAIN!!! 🚀 🚀 🚀 🚀
echo 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀; echo;